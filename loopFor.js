var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 28,
    weight : 68
}

console.log(`At the beginning of the year ${person.name} it weighs ${person.weight} Kg`);

const INCREMENT_WEIGHT = 0.2;
const DAYS_OF_THE_YEARS = 365;

const gainWeight = person => person.weight += INCREMENT_WEIGHT;
const loseWeight = person => person.weight -= INCREMENT_WEIGHT;

for(var i = 1; i <= DAYS_OF_THE_YEARS; i++) {
    var random = Math.random();

    if(random < 0.25) {
        // Gain weight
        gainWeight(person);
    }else if(random < 0.50) {
        // Lose weight
        loseWeight(person);
    }
}

console.log(`At the end of the year ${person.name} it weighs ${person.weight.toFixed(2)} Kg`);
