# JavascriptBasic

Repository based on the Platzi javascript basics course

https://platzi.com/clases/fundamentos-javascript/

Content:

1. Fundamentals
2. Control structures and functions
3. Arrays
4. Object Oriented Programming in JavaScript
5. Asynchronism
6. HTML game
7. Complements

Indications:

1. The HTML file line must be edited <script src = "example.js"> </script> to test each javascript files
