var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 28,
    engineer : true,
    chef : false,
    singer : false,
    dj : false,
    guitarist : false,
    drone : true
}

function printProfessions(person) {
    console.log(`${person.name} is:`);

    if(person.engineer) {
        console.log('engineer');
    }else{
        console.log('is not an engineer');
    }

    if(person.chef) {
        console.log('chef');
    }

    if(person.singer) {
        console.log('singer');
    }

    if(person.dj) {
        console.log('dj');
    }

    if(person.guitarist) {
        console.log('guitarist');
    }

    if(person.drone) {
        console.log('fly drone');
    }    
}

printProfessions(person);
