var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 28 
}

function printNameUppercase({name}){
    console.log(name.toUpperCase());
}

// Unstructuring objects
function printNameUppercase2(person){
    var {name} = person;
    console.log(name.toUpperCase());
}

function printNameAndAge(person){
    console.log(`My name is ${person.name} and i am ${person.age} years old`);
}

printNameUppercase(person);
printNameUppercase2(person);
printNameAndAge(person);

// Object as parameter
// Creates a new object and does not affect the global object
function birthday(person) {
    return {
        ...person,
        age: person.age + 1
    }
}

/**
 * Send object directly
 * 
 * printNameUppercase({name : 'Peter'});
 */

/**
 * Undefined case
 * 
 * printNameUppercase();
 * printNameUppercase({lastname : 'Gomez'}); 
 * 
 * */ 
