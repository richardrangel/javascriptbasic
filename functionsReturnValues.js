var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 28,
    engineer : true,
    chef : false,
    singer : false,
    dj : false,
    guitarist : false,
    drone : true
}

const FULL_AGE = 18;

function isolder(person) {
    return person.age >= FULL_AGE;
}

function printIsOlder(person) {
    if(isolder(person)){
        console.log(`${person.name} is older`);
    }else{
        console.log(`${person.name} is minor`);
    }
}

printIsOlder(person);
