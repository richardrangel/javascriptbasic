var name = 'Richie';
var lastname = 'Rangel';

//Uppercase
var uppercaseName = name.toUpperCase();

//LowerCase
var lowercaseLastname = lastname.toLowerCase();

//Capture character of a string
var firstLetterName = name.charAt(0);

//Number of characters in a string
var numberCharacterName = name.length;

//Interpolation
var fullName = `${name} ${lastname.toUpperCase()}`;

//Access certain characters in the string
var str = name.substr(1,2);

/**
 * CHALLENGE
 * know the last character of the name
 */

 var lastCharacter = name.substr((numberCharacterName - 1),1);