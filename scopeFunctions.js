name = 'Richie';

// Scope (n) --> local 
function printNameUppercase(n){
    n = n.toUpperCase();
    console.log(n);
}

// Scope (name) --> local 
function printNameUppercase2(name){
    name = name.toUpperCase();
    console.log(name);
}

printNameUppercase(name);
printNameUppercase2(name);

// Acces global variable --> window
console.log(window.name);