var x = 4, y = '4';

// Comparison between objects

var person = {
    name : 'Richie'
}

/**
 * var person2 = person
 * 
 * person === person2
 * TRUE
 * 
 * Note: person and person2 they point to the same memory address,
 * if an attribute is modified, it is reflected in the two objects
 */

 /**
 * var person2 = {
 *  ...person
 * }
 * 
 * person === person2
 * FALSE
 */
