// Increment
var age = 20;
age += 1;

// Decrement
var weight = 75;
weight -= 2;

// Addition and subtraction with variables
var sandwich = 1;
var playSoccer = 3;

weight += sandwich;
weight -= playSoccer;

// Decimals
var winePrice = 200.3;

// Math module functions
var total = Math.round(winePrice * 100 * 3) / 100;
var totalStr = total.toFixed(3);
var total2 = parseFloat(totalStr);

// Integer division
var pizza = 8;
var person = 2;
var quantityPorcionPerson = pizza / person;