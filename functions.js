var name = 'Richie', age = 28;

function printAge(n,a){
    console.log(`${n} has ${a} years`);
}

printAge(name, age);
printAge('Vicky', 29);
printAge('Mary', 26);
printAge('Peter', 29);

// Specials cases (weakly typed language)
printAge(29, 'Charly');
printAge();
printAge('Juan');
