var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 28,
    weight : 68
}

console.log(`At the beginning of the year ${person.name} it weighs ${person.weight} Kg`);

const INCREMENT_WEIGHT = 0.3;
const DAYS_OF_THE_YEARS = 365;

const gainWeight = person => person.weight += INCREMENT_WEIGHT;
const loseWeight = person => person.weight -= INCREMENT_WEIGHT;
const eatALot = () => Math.random() < 0.3;
const doesSports = () => Math.random() < 0.4;

const OBJECTIVE = person.weight - 3;
var days = 0;

while(person.weight > OBJECTIVE) {

    if(eatALot()){
        gainWeight(person);
    }

    if(doesSports()){
        loseWeight(person);
    }

    days += 1;

}

console.log(`${days} days passed until ${person.name} slimming 3 kg`);
