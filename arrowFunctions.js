var person = {
    name : 'Richie',
    lastname : 'Rangel',
    age : 17,
    engineer : true,
    chef : false,
    singer : false,
    dj : false,
    guitarist : false,
    drone : true
}

const FULL_AGE = 18;

//Arrow function
const isOlder = ({ age }) => age >= FULL_AGE;

function printIsOlder(person) {
    if(isOlder(person)){
        console.log(`${person.name} is older`);
    }else{
        console.log(`${person.name} is minor`);
    }
}

printIsOlder(person);

//Arrow function
const allowAccess = ({ age }) => !isOlder(age)

function printAcces(person) {
    if(allowAccess(person)){
        console.log('Access denied');
    }else{
        console.log('Access granted');
    }
}

printAcces(person);
